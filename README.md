Zooplus QA Exercise - Lift scenarios description (30 min)
==================================

We are testing how a lift works and we would like to have a description of the possible scenarios we can find in a lift having in mind:
- The building has 2 floors plus ground one
- An alarm in the lift is required for our building
- As security feature we would like to include the possibility of opening the doors (usual implementation)
The idea is to have a short description about how to describe the funcionalities we should implement for our amazing lift.

Zooplus QA Exercise - Automation and test-plan
==================================

Instructions
------------

What we want here?

We want to check that some features in our website are working good. As you maybe already know, we are a shop where you can buy a lot of things for your pet, so, let's check that you can start buying.

- Create a test plan (don't automate the scenarios, just define them) for our login feature (https://www.zooplus.es/account) and newsletter feature (https://www.zooplus.es/newsletter)

- Automate 2 different features: 

  * Check that a customer can add an article or articles to the cart. (You don't have to finish an order)
  
  * There is a zooPoints programm (https://www.zooplus.com/bonuspoints/overview), check that there is a tutorial inside and it's showing information. (Check it without login)

The purpose is to implement all the tests that check everything is working.

### Must have
* Tests must run and work correctly.

### Things to consider before starting
* How many tests should include?
* How are you going to write the test plan?

### How to proceed
* Fork this repository to your own account
* Implement the problem above in your own repo
* Include a solution.md file with all the relevant information for the result, such as assumptions or how to build and run the tests.
* Include a file/s with the test plans.
* Create a PR to the original repo with your solution

### Important
* You can make your own assumptions to create the tests. Please indicate them in the solution.md file.
* If you have any doubts about the problem or something that you would like us to clarify, please feel free to contact us.
